package io.github.jgabrielfreitas.as_sdk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import stone.application.StoneStart;
import stone.application.interfaces.StoneCallbackInterface;
import stone.providers.ActiveApplicationProvider;
import stone.user.UserModel;


public class MainActivity extends AppCompatActivity {

    private final String STONE_PRODUCTION_KEY = "846873720";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        List<UserModel> userModelList = StoneStart.init(this);

        if (userModelList != null && userModelList.size() > 0) {

            Toast.makeText(MainActivity.this, "App ativo", Toast.LENGTH_SHORT).show();

        } else {

            List<String> stoneCodeToActiveList = new ArrayList<String>();
            stoneCodeToActiveList.add(STONE_PRODUCTION_KEY);

            ActiveApplicationProvider activeApplicationProvider = new ActiveApplicationProvider(this, stoneCodeToActiveList); // voce deve colocar o seu StoneCode aqui
            activeApplicationProvider.setDialogMessage("Ativando o aplicativo...");
            activeApplicationProvider.setDialogTitle("Aguarde");
            activeApplicationProvider.setWorkInBackground(false); // informa se este provider ira rodar em background ou nao
            activeApplicationProvider.setConnectionCallback(new StoneCallbackInterface() {

				/* Sempre que utilizar um provider, intancie esta Interface.
				 * Ela ira lhe informar se o provider foi executado com sucesso ou nao
				 */

                /* Metodo chamado se for executado sem erros */
                public void onSuccess() {
                    Toast.makeText(getApplicationContext(), "Ativado com sucesso, iniciando o aplicativo", Toast.LENGTH_SHORT).show();
                }

                /* metodo chamado caso ocorra alguma excecao */
                public void onError() {
                    Toast.makeText(getApplicationContext(), "Erro na ativacao do aplicativo, verifique a lista de erros do provider", Toast.LENGTH_SHORT).show();
					/* Chame o metodo abaixo para verificar a lista de erros. Para mais detalhes, leia a documentacao:
					   activeApplicationProvider.getListOfErrors(); */
                }
            });
            activeApplicationProvider.execute();
        }

    }
}
